<?php
/**
 * Curse Inc.
 * GlobalBlock
 * GlobalBlock Aliases
 *
 * @author 		Alexia E. Smith
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		GlobalBlock
 * @link		https://gitlab.com/hydrawiki
 *
**/

$specialPageAliases = [];

/** English (English) */
$specialPageAliases['en'] = [
	'GlobalBlockList' => ['GlobalBlockList']
];

/** Deutsch (German) */
$specialPageAliases['de'] = [
	'GlobalBlockList' => ['Globale Sperrliste', 'Liste der globalen Sperren', 'Liste globaler Sperren']
];

/** polski (Polish) */
$specialPageAliases['pl'] = [
    'GlobalBlockList' => [
        'Globalnie zablokowani użytkownicy',
        'Globalna lista blokad',
        'Blokady globalne'
    ]
];