<?php
/**
 * Curse Inc.
 * Global Block
 * Manage global blocks for anonymous and registered accounts.
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		GlobalBlock
 * @link		https://gitlab.com/hydrawiki
 *
**/

use MediaWiki\MediaWikiServices;

class GlobalBlock {
	/**
	 * Global Block Data
	 *
	 * @var	array
	 */
	 private $data = [
			'gbid'					=> null,
			'target'				=> '',
			'user_id'				=> 0,
			'performer_user_id'		=> 0,
			'performer_name'		=> '',
			'reason'				=> '',
			'timestamp'				=> 0,
			'enable_autoblock'		=> 0,
			'expiry'				=> '',
			'ipb_range_start'		=> '',
			'ipb_range_end'			=> '',
			'deleted'				=> 0,
			'blocks_create_account'	=> 0,
			'blocks_email'			=> 0,
			'blocks_edit_usertalk'	=> 0,
			'site_key'				=> null,
			'wiki_name'				=> null
	 ];

	/**
	 * Local User object that is being blocked.
	 *
	 * @var	object
	 */
	 private $target = null;

	/**
	 * Local User object that performed the block.
	 * May not be available on the local wiki.
	 *
	 * @var	object
	 */
	 private $performer = null;

	/**
	 * Global block database connections.
	 *
	 * @var	object
	 */
	 static private $dbs = [];

	/**
	 * Initialize a new object from a local block.
	 *
	 * @access	public
	 * @param	object	User Block
	 * @return	mixed	New GlobalBlock or false on error.
	 */
	static public function newFromBlock(Block $block) {
		global $dsSiteKey, $wgSitename;

		$globalBlock = new self();

		$target = $block->getTarget();
		$performer = $block->getBlocker();
		if (!$target instanceof User || !$performer instanceof User) {
			//Required to create a global block.
			return false;
		}
		$globalBlock->target = $target;
		$globalBlock->performer = $performer;

		$blockType = Block::parseTarget($target);

		$globalBlock->data['gbid']					= null;
		$globalBlock->data['target']				= ($target !== null ? $target->getName() : '');
		$globalBlock->data['user_id']				= $target->getId();
		$globalBlock->data['performer_user_id']		= $performer->getId();
		$globalBlock->data['performer_name']		= $performer->getName();
		$globalBlock->data['reason']				= $block->mReason;
		$globalBlock->data['timestamp']				= $block->mTimestamp;
		$globalBlock->data['enable_autoblock']		= intval($block->isAutoblocking());
		$globalBlock->data['expiry']				= $block->getExpiry();
		$globalBlock->data['ipb_range_start']		= $block->getRangeStart();
		$globalBlock->data['ipb_range_end']			= $block->getRangeEnd();
		$globalBlock->data['deleted']				= intval($block->mHideName);
		$globalBlock->data['blocks_create_account']	= intval($block->prevents('createaccount'));
		$globalBlock->data['blocks_email']			= intval($block->prevents('sendemail'));
		$globalBlock->data['blocks_edit_usertalk']	= intval($block->prevents('editownusertalk'));
		$globalBlock->data['site_key']				= $dsSiteKey;
		$globalBlock->data['wiki_name']				= $wgSitename;

		if (!$globalBlock->load()) {
			return false;
		}

		return $globalBlock;
	}

	/**
	 * Initialize a new object from a local block.
	 *
	 * @access	public
	 * @param	object	User
	 * @return	mixed	New GlobalBlock or false on error.
	 */
	static public function newFromUser(User $target) {
		global $dsSiteKey, $wgSitename;

		$globalBlock = new self();

		if (!$target instanceof User) {
			return false;
		}
		$globalBlock->target = $target;

		$globalBlock->data['gbid']					= null;
		$globalBlock->data['target']				= ($target !== null ? $target->getName() : '');
		$globalBlock->data['user_id']				= $target->getId();
		$globalBlock->data['timestamp']				= wfTimestamp(TS_MW);
		$globalBlock->data['site_key']				= $dsSiteKey;
		$globalBlock->data['wiki_name']				= $wgSitename;

		if (!$globalBlock->load()) {
			return false;
		}

		return $globalBlock;
	}

	/**
	 * Initialize a new object from a local block.
	 *
	 * @access	public
	 * @param	object	User
	 * @return	mixed	New GlobalBlock or false on error.
	 */
	static public function newFromRow($row) {
		$globalBlock = new self();

		if (!is_array($row)) {
			$row = (array)$row;
		}

		$globalBlock->data['gbid']					= intval($row['gbid']);
		$globalBlock->data['target']				= $row['target'];
		$globalBlock->data['user_id']				= intval($row['user_id']);
		$globalBlock->data['performer_user_id']		= intval($row['performer_user_id']);
		$globalBlock->data['performer_name']		= $row['performer_name'];
		$globalBlock->data['reason']				= $row['reason'];
		$globalBlock->data['timestamp']				= $row['timestamp'];
		$globalBlock->data['enable_autoblock']		= intval($row['enable_autoblock']);
		$globalBlock->data['expiry']				= $row['expiry'];
		$globalBlock->data['ipb_range_start']		= $row['ipb_range_start'];
		$globalBlock->data['ipb_range_end']			= $row['ipb_range_end'];
		$globalBlock->data['deleted']				= intval($row['deleted']);
		$globalBlock->data['blocks_create_account']	= intval($row['blocks_create_account']);
		$globalBlock->data['blocks_email']			= intval($row['blocks_email']);
		$globalBlock->data['blocks_edit_usertalk']	= intval($row['blocks_edit_usertalk']);
		$globalBlock->data['site_key']				= $row['site_key'];
		$globalBlock->data['wiki_name']				= $row['wiki_name'];

		return $globalBlock;
	}

	/**
	 * Load existing block information.
	 *
	 * @access	public
	 * @return	boolean	Success
	 */
	public function load() {
		$dbr = self::getMasterDatabase(DB_REPLICA);
		if (!$dbr) {
			return false;
		}

		$where = [];
		if ($this->data['user_id']) {
			$where['user_id'] = $this->data['user_id'];
		} else {
			$where['target'] = $this->data['target'];
		}

		$result = $dbr->select(
			['global_block'],
			['*'],
			$where,
			__METHOD__
		);
		$exists = $result->fetchRow();

		if (isset($exists['gbid']) && $exists['gbid'] > 0) {
			$this->data['gbid'] = $exists['gbid'];
			$this->data = array_merge($this->data, $exists);
		}
		return true;
	}

	/**
	 * Save this block.
	 *
	 * @access	public
	 * @return	boolean	Success
	 */
	public function save() {
		$dbw = self::getMasterDatabase(DB_MASTER);
		if (!$dbw) {
			return false;
		}

		if (isset($this->data['gbid']) && $this->data['gbid'] > 0) {
			$success = $dbw->update(
				'global_block',
				$this->data,
				['gbid' => $this->data['gbid']],
				__METHOD__
			);
		} else {
			$success = $dbw->insert(
				'global_block',
				$this->data,
				__METHOD__
			);
			$this->data['gbid'] = $dbw->insertId();
		}

		return $success;
	}

	/**
	 * Delete this block.
	 *
	 * @access	public
	 * @return	boolean	Success
	 */
	public function delete() {
		$dbw = self::getMasterDatabase(DB_MASTER);
		if (!$dbw) {
			return false;
		}

		if (isset($this->data['gbid']) && $this->data['gbid'] > 0) {
			$success = $dbw->delete(
				'global_block',
				['gbid' => $this->data['gbid']],
				__METHOD__
			);
		}

		return $success;
	}

	/**
	 * Get identifier.
	 *
	 * @access	public
	 * @return	integer	Database ID
	 */
	public function getId() {
		return intval($this->data['gbid']);
	}

	/**
	 * Get the intended target of this block.
	 *
	 * @access	public
	 * @return	string	Target
	 */
	public function getTarget() {
		return $this->data['target'];
	}

	/**
	 * Does this block exist?
	 *
	 * @access	public
	 * @return	boolean	Exists
	 */
	public function exists() {
		return boolval($this->getId());
	}

	/**
	 * Block currently enforced?
	 *
	 * @access	public
	 * @return	boolean	Expired
	 */
	public function isExpired() {
		$timestamp = wfTimestampNow();

		if (!$this->data['expiry']) {
			return false;
		}

		return $timestamp > $this->data['expiry'];
	}

	/**
	 * Is deleted?
	 *
	 * @access	public
	 * @return	boolean	Blocked
	 */
	public function isDeleted() {
		return boolval($this->data['deleted']);
	}

	/**
	 * Get block reason.
	 *
	 * @access	public
	 * @param	boolean	Parse the message or leave it plain.
	 * @return	string	Reason
	 */
	public function getReason($parse = true) {
		$blockListUrl = $this->getWikiUrlBase();
		if (!empty($blockListUrl)) {
			$blockListUrl .= "/Special:BlockList";
		}

		if (empty($this->data['reason'])) {
			$message = wfMessage('global_block_noreason', $this->getId(), $this->getPerformerName(), $this->getWikiName(), $blockListUrl);
		} else {
			$message = wfMessage('global_block_reason', $this->getId(), $this->getPerformerName(), $this->getWikiName(), $blockListUrl, $this->data['reason']);
		}

		if ($parse) {
			return $message->parse();
		}
		return $message->plain();
	}

	/**
	 * Get the wiki name this block was created on.
	 *
	 * @access	public
	 * @return	string	Wiki Name
	 */
	public function getWikiName() {
		$wiki = self::getWikiInfoFromCache($this->data['site_key']);
		if (isset($wiki['wiki_name_display']) && !empty($wiki['wiki_name_display'])) {
			return $wiki['wiki_name_display'];
		}

		return $this->data['wiki_name'];
	}

	/**
	 * Get the wiki base URL for the wiki this block was created on.
	 *
	 * @access	public
	 * @return	string	Wiki Base URL
	 */
	public function getWikiUrlBase() {
		global $wgScriptPath;

		$wiki = self::getWikiInfoFromCache($this->data['site_key']);
		if (isset($wiki['wiki_domain']) && !empty($wiki['wiki_domain'])) {
			return wfExpandUrl('https://'.$wiki['wiki_domain'].$wgScriptPath, PROTO_HTTPS);
		}

		return $wgScriptPath;
	}

	/**
	 * Get the raw performer name.
	 *
	 * @access	public
	 * @return	string	Performer Name
	 */
	public function getPerformerName() {
		if ($this->performer === null) {
			$user = $this->getPerformer();
			if ($user !== false) {
				return $user->getName();
			}
		}

		$blockingUserName = $this->data['performer_name'];
		
		if ($this->getPerformer() === false) {
			$configfactory = MediaWikiServices::getInstance()->getConfigFactory();
			$config = $configfactory->makeConfig('main');
			$remoteInterwiki = $config->get('GlobalBlockRemoteUserInterwiki');
			$blockingUserName = $remoteInterwiki . ">" . $blockingUserName;
		}

		return $blockingUserName;
	}

	/**
	 * Get the local user object for the performer, if available.
	 *
	 * @access	public
	 * @return	mixed	User object or false if not available.
	 */
	public function getPerformer() {
		if ($this->data['performer_user_id']) {
			$user = User::newFromId($this->data['performer_user_id']);
			if ($user !== null && $user->getId() > 0) {
				$this->performer = $user;
				return $user;
			}
		}

		$this->performer = false;

		return false;
	}

	/**
	 * Set the block performer.
	 *
	 * @access	public
	 * @param	object	User
	 * @return	boolean	Success
	 */
	public function setPerformer(User $performer) {
		if (!$performer instanceof User) {
			return false;
		}

		$this->data['performer_user_id']	= $performer->getId();
		$this->data['performer_name']		= $performer->getName();

		return true;
	}

	/**
	 * Get a fake local block.
	 *
	 * @access	public
	 * @return	object	Block
	 */
	public function getFakeLocalBlock() {
		$performer = $this->getPerformer();

		$block = new Block(
			[
				'address'         => $this->target,
				'by'              => ($performer ? $performer->getId() : null),
				'reason'          => $this->getReason(false),
				'timestamp'       => $this->data['timestamp'],
				'auto'            => false,
				'expiry'          => $this->data['expiry'],
				'createAccount'   => $this->prevents('blocks_create_account'),
				'enableAutoblock' => $this->data['enable_autoblock'],
				'hideName'        => $this->isDeleted(),
				'blockEmail'      => $this->prevents('blocks_email'),
				'allowUsertalk'   => !$this->prevents('blocks_edit_usertalk'),
				'byText'          => $this->getPerformerName(),
			]
		);

		return $block;
	}

	/**
	 * Get/set whether the Block prevents a given action
	 *
	 * @access	public
	 * @param	string	$action Action to check
	 * @param	mixed	$x Value for set, or null to just get value
	 * @return	mixed	Null for unrecognized rights.
	 */
	public function prevents($action, $x = null) {
		global $wgBlockDisablesLogin;
		$res = null;

		switch ($action) {
			case 'edit':
				$res = true;
				break;
			case 'read':
				$res = false;
				break;
			default:
				if (strpos($action, 'blocks_') === 0) {
					$res = $this->data[$action];
					if ($x !== null) {
						$this->data[$action] = $x;
					}
				}
				break;
		}
		if (!$res && $wgBlockDisablesLogin) {
			// If a block would disable login, then it should
			// prevent any action that all users cannot do
			$anon = new User;
			$res = $anon->isAllowed($action) ? $res : true;
		}

		return $res;
	}

	/**
	 * Get the global database connection.
	 *
	 * @access	public
	 * @param	integer	$dbtype either DB_MASTER or DB_SLAVE.
	 * @return	mixed	DatabaseBase or false on connection error.
	 */
	static public function getMasterDatabase($dbtype = DB_MASTER) {
		$config = \ConfigFactory::getDefaultInstance()->makeConfig('main');
		$wgGlobalBlockingCluster = $config->get('GlobalBlockingCluster');
		$wgGlobalBlockingDatabase = $config->get('GlobalBlockingDatabase');
		if ($wgGlobalBlockingCluster === false || $wgGlobalBlockingDatabase === false) {
			return false;
		}

		if (isset(self::$dbs[$dbtype]) && self::$dbs[$dbtype] !== null) {
			return self::$dbs[$dbtype];
		}

		try {
			self::$dbs[$dbtype] = MediaWikiServices::getInstance()->getDBLoadBalancerFactory()->getExternalLB($wgGlobalBlockingCluster)->getConnection($dbtype, false, $wgGlobalBlockingDatabase);
		} catch (Exception $e) {
			\MWExceptionHandler::logException($e);
			self::$dbs[$dbtype] = false;
		}
		return self::$dbs[$dbtype];
	}

	/**
	 * Get wiki information from the Redis cache.
	 *
	 * @access		public
	 * @param		string	Wiki Database Name
	 * @return		array
	 */
	static public function getWikiInfoFromCache($siteKey) {
		$info = [];
		if (!is_string($siteKey) || empty($siteKey)) {
				return $info;
		}
		$redis = \RedisCache::getClient('cache', [], true);

		if (!$redis) {
			return [];
		}

		try {
			$info = $redis->hGetAll('dynamicsettings:siteInfo:'.$siteKey);
			if (!empty($info)) {
				foreach ($info as $field => $value) {
					$info[$field] = unserialize($value);
				}
			}
		} catch (\RedisException $e) {
				wfDebug(__METHOD__.": Caught RedisException - ".$e->getMessage());
		}
		return $info;
	}
}
