<?php
/**
 * Replace Global ID with User ID Maintenance Script
 *
 * @package   GlobalBlock
 * @copyright (c) 2020 Curse Inc.
 * @license   GPL-2.0-or-later
 * @link      https://gitlab.com/hydrawiki
**/

namespace GlobalBlock\Maintenance;

use HydraAuthUser;
use LoggedUpdateMaintenance;
use Wikimedia\Rdbms\IDatabase;

require_once dirname(dirname(dirname(dirname(__DIR__)))) . '/maintenance/Maintenance.php';

/**
 * Maintenance script that cleans up tables that have orphaned users.
 */
class ReplaceGlobalIdWithUserId extends LoggedUpdateMaintenance {
	private $prefix;

	private $table;

	/**
	 * Constructor
	 */
	public function __construct() {
		parent::__construct();
		$this->addDescription('Replaces global ID with user ID in the global_block table.');
		$this->setBatchSize(100);
	}

	/**
	 * Return an unique name to logged this maintenance as being done.
	 *
	 * @return string
	 */
	protected function getUpdateKey() {
		return __CLASS__;
	}

	/**
	 * Do database updates for all tables.
	 *
	 * @return boolean True
	 */
	protected function doDBUpdates() {
		$this->cleanup();

		return true;
	}

	/**
	 * Cleanup the global_block table.
	 *
	 * @return void
	 */
	protected function cleanup() {
		$table = 'global_block';
		$primaryKey = 'gbid';
		$orderby = ['gbid'];
		$globalIdFields = [
			'global_id' => 'user_id',
			'performer_global_id' => 'performer_user_id'
		];

		$dbw = $this->getDB(DB_MASTER);

		foreach ($globalIdFields as $key => $value) {
			if (!$dbw->fieldExists($table, $key)) {
				unset($globalIdFields[$key]);
			}
		}
		if (empty($globalIdFields)) {
			$this->output("Skipping due to global ID fields not being present.\n");
			return;
		}

		$this->output(
			"Beginning cleanup of $table\n"
		);

		$next = '1=1';
		$count = 0;
		while (true) {
			// Fetch the rows needing update
			$res = $dbw->select(
				$table,
				[
					$primaryKey,
					'global_id',
					'performer_global_id',
					'user_id',
					'performer_user_id'
				],
				[$next],
				__METHOD__,
				[
					'ORDER BY' => $orderby,
					'LIMIT' => $this->mBatchSize,
				]
			);
			if (!$res->numRows()) {
				break;
			}

			// Update the existing rows
			foreach ($res as $row) {
				$userId = HydraAuthUser::userIdFromGlobalId($row->global_id);
				$performerUserId = HydraAuthUser::userIdFromGlobalId($row->performer_global_id);

				$delete = false;
				if (!$userId || !$performerUserId) {
					$delete = true;
				}

				if ($delete) {
					$dbw->delete(
						'global_block',
						['gbid' => $row->gbid],
						__METHOD__
					);
				} else {
					$dbw->update(
						$table,
						[
							'user_id' => $userId,
							'performer_user_id' => $performerUserId
						],
						['gbid' => $row->gbid],
						__METHOD__
					);
				}
				$count += $dbw->affectedRows();
			}

			list($next, $display) = $this->makeNextCond($dbw, $orderby, $row);
			$this->output("... $display\n");
			wfWaitForSlaves();
		}

		$this->output(
			"Cleanup complete: Replaced {$count} global IDs with user ID\n"
		);
	}

	/**
	 * Calculate a "next" condition and progress display string
	 *
	 * @param IDatabase $dbw
	 * @param string[]  $indexFields Fields in the index being ordered by
	 * @param object    $row         Database row
	 *
	 * @return string[] [ string $next, string $display ]
	 */
	private function makeNextCond($dbw, $indexFields, $row) {
		$next = '';
		$display = [];
		for ($i = count($indexFields) - 1; $i >= 0; $i--) {
			$field = $indexFields[$i];
			$display[] = $field . '=' . $row->$field;
			$value = $dbw->addQuotes($row->$field);
			if ($next === '') {
				$next = "$field > $value";
			} else {
				$next = "$field > $value OR $field = $value AND ($next)";
			}
		}
		$display = implode(' ', array_reverse($display));
		return [$next, $display];
	}
}

$maintClass = \GlobalBlock\Maintenance\ReplaceGlobalIdWithUserId::class;
require_once RUN_MAINTENANCE_IF_MAIN;
