CREATE TABLE /*_*/global_block (
  `gbid` int(11) NOT NULL,
  `target` tinyblob NOT NULL,
  `user_id` int(16) UNSIGNED NOT NULL DEFAULT '0',
  `performer_user_id` int(16) UNSIGNED NOT NULL DEFAULT '0',
  `performer_name` varbinary(255) NOT NULL DEFAULT '',
  `reason` varbinary(767) NOT NULL,
  `timestamp` binary(14) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `auto` tinyint(1) NOT NULL DEFAULT '0',
  `enable_autoblock` tinyint(1) NOT NULL DEFAULT '1',
  `expiry` varbinary(14) NOT NULL DEFAULT '',
  `ipb_range_start` tinyblob NOT NULL,
  `ipb_range_end` tinyblob NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `blocks_create_account` tinyint(1) NOT NULL DEFAULT '1',
  `blocks_email` tinyint(1) NOT NULL DEFAULT '1',
  `blocks_edit_usertalk` tinyint(1) NOT NULL DEFAULT '1',
  `site_key` varchar(32) DEFAULT NULL,
  `wiki_name` varchar(255) DEFAULT NULL
) /*$wgDBTableOptions*/;

ALTER TABLE /*_*/global_block
  ADD PRIMARY KEY (`gbid`),
  ADD UNIQUE KEY `target_user_id` (`target`(255),`user_id`),
  ADD KEY `timestamp` (`timestamp`),
  ADD KEY `expiry` (`expiry`),
  ADD KEY `site_key` (`site_key`),
  ADD KEY `ipb_range` (`ipb_range_start`(8),`ipb_range_end`(8)),
  ADD KEY `user_id` (`user_id`) USING BTREE;

ALTER TABLE /*_*/global_block MODIFY `gbid` int(11) NOT NULL AUTO_INCREMENT;