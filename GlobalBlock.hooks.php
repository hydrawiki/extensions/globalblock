<?php
/**
 * Curse Inc.
 * Global Block
 * Manage global blocks for anonymous and registered accounts.
 *
 * @author		Alexia E. Smith
 * @copyright	(c) 2017 Curse Inc.
 * @license		GNU General Public License v2.0 or later
 * @package		GlobalBlock
 * @link		https://gitlab.com/hydrawiki
 *
**/

use DynamicSettings\Environment;

class GlobalBlockHooks {
	/**
	 * Add global block option to form fields.
	 *
	 * @acess	public
	 * @param	object	SpecialBlock
	 * @param	array	Existing form field descriptors.
	 * @return	boolean	True
	 */
	static public function onSpecialBlockModifyFormFields($specialPage, &$fields) {
		global $wgUser;

		if ($wgUser !== null && $wgUser->isAllowed('globalblock')) {
			$fields['GlobalBlock'] = [
				'type' => 'check',
				'label-message' => 'gb_modifyglobal',
				'default' => true,
			];
		}

		return true;
	}

	/**
	 * Handle modifying global blocks when creating or updating a block.
	 *
	 * @acess	public
	 * @param	object	Block
	 * @param	object	User performing the block.
	 * @return	boolean	True
	 */
	static public function onBlockIpComplete(Block $block, User $user) {
		global $dsSiteKey;

		$request = RequestContext::getMain()->getRequest();
		$isPartial = $request->getVal('wpEditingRestriction') === 'partial';

		if (!$isPartial && $user !== null && $user->isAllowed('globalblock') && $request->getBool('wpGlobalBlock', false)) {
			$globalBlock = GlobalBlock::newFromBlock($block);
			if ($globalBlock !== false) {
				$globalBlock->save();
			}
		}

		return true;
	}

	/**
	 * Handle modifying global blocks when removing a block.
	 *
	 * @acess	public
	 * @param	object	Block
	 * @param	object	User performing the unblock.
	 * @return	boolean	True
	 */
	static public function onUnblockUserComplete(Block $block, User $user) {
		$request = RequestContext::getMain()->getRequest();
		if ($user !== null && $user->isAllowed('globalblock')) {
			$globalBlock = GlobalBlock::newFromBlock($block);
			if ($globalBlock !== false) {
				$globalBlock->delete();
			}
		}

		return true;
	}

	/**
	 * Get blocked status on the global level.
	 *
	 * @acess	public
	 * @param	object	User being tested for being blocked.
	 * @return	boolean	True
	 */
	static public function onGetBlockedStatus(User $user) {
		if ($user !== null) {
			$globalBlock = GlobalBlock::newFromUser($user);
			if ($globalBlock !== false && $globalBlock->exists() && !$globalBlock->isExpired()) {
				$block = $globalBlock->getFakeLocalBlock();
				if ($block !== null) {
					$user->mBlock = $block;
					$user->mBlockedby = $globalBlock->getPerformerName();
				}
			}
		}

		return true;
	}

	/**
	 * Setups and Modifies Database Information
	 *
	 * @access	public
	 * @param	object	[Optional] DatabaseUpdater Object
	 * @return	boolean	True
	 */
	static public function onLoadExtensionSchemaUpdates(DatabaseUpdater $updater = null) {
		$extDir = __DIR__;

		//Install
		//Tables
		if (Environment::isMasterWiki()) {
			$updater->addExtensionUpdate(['addTable', 'global_block', "{$extDir}/install/sql/table_global_block.sql", true]);
			$updater->addExtensionUpdate(['modifyField', 'global_block', "ip_range_start", "{$extDir}/upgrade/sql/global_block/rename_ip_range_start.sql", true]);
			$updater->addExtensionUpdate(['modifyField', 'global_block', "ip_range_end", "{$extDir}/upgrade/sql/global_block/rename_ip_range_end.sql", true]);
			$updater->addExtensionUpdate(['dropIndex', 'global_block', "ip_range", "{$extDir}/upgrade/sql/global_block/drop_index_ip_range.sql", true]);
			$updater->addExtensionUpdate(['addIndex', 'global_block', "ipb_range", "{$extDir}/upgrade/sql/global_block/add_index_ipb_range.sql", true]);
			$updater->addExtensionUpdate(['addField', 'global_block', "auto", "{$extDir}/upgrade/sql/global_block/add_field_auto.sql", true]);
			$updater->addExtensionUpdate(['addField', 'global_block', "user_id", "{$extDir}/upgrade/sql/global_block/add_field_user_id.sql", true]);
			$updater->addExtensionUpdate(['addField', 'global_block', "performer_user_id", "{$extDir}/upgrade/sql/global_block/add_field_performer_user_id.sql", true]);
			$updater->addExtensionUpdate(['addIndex', 'global_block', "user_id", "{$extDir}/upgrade/sql/global_block/add_index_user_id.sql", true]);

			// Global ID drop.
			$updater->addExtensionUpdate(['addIndex', 'global_block', "target_user_id", "{$extDir}/upgrade/sql/global_block/add_index_target_user_id.sql", true]);
			$updater->addExtensionUpdate(['dropIndex', 'global_block', "target", "{$extDir}/upgrade/sql/global_block/drop_index_target.sql", true]);
			$updater->addExtensionUpdate(['dropField', 'global_block', "global_id", "{$extDir}/upgrade/sql/global_block/drop_field_global_id.sql", true]);
			$updater->addExtensionUpdate(['dropField', 'global_block', "performer_global_id", "{$extDir}/upgrade/sql/global_block/drop_field_performer_global_id.sql", true]);

			$updater->addPostDatabaseUpdateMaintenance(\GlobalBlock\Maintenance\ReplaceGlobalIdWithUserId::class);
		}

		return true;
	}
}
